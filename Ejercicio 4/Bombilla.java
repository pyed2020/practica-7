
public class Bombilla {
	private boolean estado;
	
	public Bombilla (boolean estado) {
		this.estado = estado;
	}
	
	public void activar () {
		this.estado = true;
	}
	
	public void desactivar () {
		this.estado = false; 
	}
}
