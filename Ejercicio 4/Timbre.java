
public class Timbre {
	private boolean estado;
	
	public Timbre (boolean estado) {
		this.estado = estado;
	}
	
	public void activar () {
		this.estado = true;
	}
	
	public void desactivar () {
		this.estado = false; 
	}

}
