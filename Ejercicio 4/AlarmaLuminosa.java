
public class AlarmaLuminosa extends Alarma{
	private Bombilla bombilla;

	public AlarmaLuminosa(double umbral, Sensor sensor, Timbre timbre, Bombilla bombilla) {
		super(umbral, sensor, timbre);
		this.bombilla = bombilla;
	}
	
	public void comprobar () {
		super.comprobar();
		if (umbral > sensor.getValorActual()) {
			bombilla.activar();
			return;
		}
		bombilla.activar();
	}

}
