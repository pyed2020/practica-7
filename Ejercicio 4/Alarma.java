
public class Alarma {
	
	protected double umbral;
	protected Sensor sensor;
	protected Timbre timbre;
	
	public Alarma (double umbral, Sensor sensor, Timbre timbre) {
		this.umbral = umbral;
		this.sensor = sensor;
		this.timbre = timbre;
	}
	
	public void comprobar () {
		if (umbral > sensor.getValorActual()) {
			timbre.activar();
			return;
		}
		timbre.desactivar();
	}
}
