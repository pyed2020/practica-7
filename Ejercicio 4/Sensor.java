
public class Sensor {
	private double valorActual;
	
	public Sensor (double valorActual) {
		this.valorActual=valorActual;
	}

	public double getValorActual() {
		return valorActual;
	}
}
