
public class Punto {
	private double x;
	private double y;
	
	public Punto (double x, double y) {
		this.x = x;
		this.y =y;
	}
	public int cuadrante () {
		if (x>=0 && y>=0) {
			return 1;
		}else if (x>=0 && y<0){
			return 2;
		} else if (x<0 && y<0) {
			return 3;
		} else {
			return 4;
		}
	}
	public void setX(double x) {
		this.x = x;
	}
	public void setY(double y) {
		this.y = y;
	}
	
	@Override
	public String toString() {
		return ("("+x+","+y+")");
	}
}
