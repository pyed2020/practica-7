
public class Manzana {
	private String raza;
	private int precio;
	
	public Manzana(String raza, int precio) {
		this.setRaza(raza);
		this.setPrecio(precio);
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}public void setRaza(String raza) {
		this.raza = raza;
	}public int getPrecio() {
		return precio;
	}public String getRaza() {
		return raza;
	}
	
}
