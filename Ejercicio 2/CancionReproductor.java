import javax.sound.sampled.Clip;

public class CancionReproductor {
	private String titulo;
	private String autor;
	private Clip audio;
	
	public CancionReproductor (String titulo, String autor, Clip audio) {
		this.setAudio(audio);
		this.setAutor(autor);
		this.setTitulo(titulo);
	}
	public void setAudio(Clip audio) {
		this.audio = audio;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Clip getAudio() {
		return audio;
	}
	public String getAutor() {
		return autor;
	}
	public String getTitulo() {
		return titulo;
	}
}
