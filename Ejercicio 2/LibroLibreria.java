
public class LibroLibreria {

	private String titulo;
	private String autor;
	private int codigo;
	private String texto;
	private int precio;
	
	public LibroLibreria(String titulo, String autor, int codigo, String texto, int precio) {
		this.setTitulo(titulo);
		this.setAutor(autor);
		this.setCodigo(codigo);
		this.setTexto(texto);
		this.setPrecio(precio);
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public int getCodigo() {
		return codigo;
	}
	public String getTexto() {
		return texto;
	}
	public String getTitulo() {
		return titulo;
	}
	public int getPrecio() {
		return precio;
	}
}
