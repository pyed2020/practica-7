/**
 * Supongo que el espacio cartesiano es de 2 dimensiones
 */
public class Punto {
    private double x;
    private double y;

    public Punto(double x,double y) {
        this.setX(x);
        this.setY(y);
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

}