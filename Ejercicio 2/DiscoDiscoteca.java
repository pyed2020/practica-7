
public class DiscoDiscoteca {
	private double duracion;
	private String nombre;
	private String autor;
	private String contenido;
	
	public DiscoDiscoteca(double duracion, String autor, String nombre, String contenido) {
		this.setDuracion(duracion);
		this.setAutor(autor);
		this.setNombre(nombre);
		this.setContenido(contenido);
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setDuracion(double duracion) {
		this.duracion = duracion;
	}
	
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	
	public String getAutor() {
		return autor;
	}
	public String getNombre() {
		return nombre;
	}
	
	public String getContenido() {
		return contenido;
	}
	public double getDuracion() {
		return duracion;
	}
	
}


