
public class TelefonoTienda {
	private double precio;
	private String marca;
	private String modelo;
	
	public TelefonoTienda(double precio, String marca, String modelo) {
		this.setPrecio(precio);
		this.setMarca(marca);
		this.setModelo(modelo);
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public String getMarca() {
		return marca;
	}
	public String getModelo() {
		return modelo;
	}
	public double getPrecio() {
		return precio;
	}
}
