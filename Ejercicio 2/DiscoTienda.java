
public class DiscoTienda {
	private double precio;
	private String autor;
	private String nombre;
	
	public DiscoTienda(double precio, String autor, String nombre) {
		this.setPrecio(precio);
		this.setAutor(autor);
		this.setNombre(nombre);
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	public String getAutor() {
		return autor;
	}
	public String getNombre() {
		return nombre;
	}
	public double getPrecio() {
		return precio;
	}
}
