
public class Carta {
	private String origen;
	private String destino;
	private String texto;
	public Carta (String origen, String destino, String texto) {
		this.setOrigen(origen);
		this.setDestino(destino);
		this.setTexto(texto);
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}public void setOrigen(String origen) {
		this.origen = origen;
	}public void setTexto(String texto) {
		this.texto = texto;
	}public String getDestino() {
		return destino;
	}public String getOrigen() {
		return origen;
	}public String getTexto() {
		return texto;
	}
}
