
public class LibroBiblioteca {
	private String titulo;
	private String autor;
	private int codigo;
	private String texto;
	private boolean disponible;
	
	public LibroBiblioteca(String titulo, String autor, int codigo, String texto, boolean disponible) {
		this.setTitulo(titulo);
		this.setAutor(autor);
		this.setCodigo(codigo);
		this.setTexto(texto);
		this.setDisponible(disponible);
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public int getCodigo() {
		return codigo;
	}
	public String getTexto() {
		return texto;
	}
	public String getTitulo() {
		return titulo;
	}
	public boolean isDisponible() {
		return disponible;
	}
}
