//import javax.sound.sampled.Clip;
//A partir de java 15 funcionaría con Clip

public class CancionRadio {
	private String titulo;
	private String autor;
	private String audio;
	
	public CancionRadio (String titulo, String autor, String audio) {
		this.setAudio(audio);
		this.setAutor(autor);
		this.setTitulo(titulo);
	}
	public void setAudio(String audio) {
		this.audio = audio;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAudio() {
		return audio;
	}
	public String getAutor() {
		return autor;
	}
	public String getTitulo() {
		return titulo;
	}
}