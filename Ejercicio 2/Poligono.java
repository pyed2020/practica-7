import java.util.ArrayList;

public class Poligono {
	private ArrayList <Segmento> segmentos;
	private ArrayList <Double> angulos;
	
	public Poligono (ArrayList <Segmento> segmentos, ArrayList <Double> angulos) {
		this.setSegmentos(segmentos);
		this.setAngulos (angulos);
	}
	public void setSegmentos(ArrayList<Segmento> segmentos) {
		this.segmentos = segmentos;
	}
	public void setAngulos(ArrayList<Double> angulos) {
		this.angulos = angulos;
	}public ArrayList<Double> getAngulos() {
		return angulos;
	}public ArrayList<Segmento> getSegmentos() {
		return segmentos;
	}
}

 