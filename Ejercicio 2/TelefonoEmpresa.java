
public class TelefonoEmpresa {
	private int numTelefono;
	private String marca;
	private String propietario;
	private String modelo;
	
	public TelefonoEmpresa (int numTelefono, String marca, String propietario, String modelo) {
		this.setNumTelefono(numTelefono);
		this.setMarca(marca);
		this.setPropietario(propietario);
		this.setModelo(modelo);
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public void setNumTelefono(int numTelefono) {
		this.numTelefono = numTelefono;
	}
	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}
	public String getMarca() {
		return marca;
	}
	public String getModelo() {
		return modelo;
	}
	public int getNumTelefono() {
		return numTelefono;
	}
	public String getPropietario() {
		return propietario;
	}
}