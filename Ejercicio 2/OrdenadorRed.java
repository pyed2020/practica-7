
public class OrdenadorRed {
	private String IPpublica;
	private String MAC;
	public OrdenadorRed(String IPpublica, String MAC) {
		this.setIPpublica(IPpublica);
		this.setMAC(MAC);
	}
	public void setIPpublica(String iPpublica) {
		IPpublica = iPpublica;
	}
	public void setMAC(String mAC) {
		MAC = mAC;
	}
	public String getIPpublica() {
		return IPpublica;
	}
	public String getMAC() {
		return MAC;
	}
}
